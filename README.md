## A simple SAT solver

---

Here you can find an implementation of a simple SET solver that was created as
a part of a Logic in Computer Science course at the Faculty of Mathematics and
Physics - University of Ljubljana.

### Content

The repository contains:

- SS4 - A simple SAT solver written in SML programming language.
- SS5 - A simple SAT solver written in C++ programming language. 
- check_if - A python script that can verifying if a valuation satisfies a
logical formula.

#### SS4

An implementation on a DPLL algorithm in SML programming language.

#### SS5 - main solution

An implementation of a modified DPLL algorith in C++ programming language with the following modifications:

- Head and Tail lazy data structure - It Minimizes the number of steps needed to assign a new value to a variable and to find a conflict. It simplifies backtracking and reduce the memory usage.
- Random selection of the next literal - On a branching step the literal is randomly selected.
- Restart strategy - A restart strategy and a random selection of the branching literal reduce the heavy-tailed behavior of the DPLL algorithm. The restart strategy is bound to the number of backtracking steps taken in a single search. The search is restarted when the number of backtracking steps reaches the selected value. The selected value is exponentially increased to allow the algorithm to explore larger solutions.
- Parallelism - The solution uses multiple threads to explore the space of possible solutions.

How to use:

```
$ cd SS5
$ mkdir build
$ cd build
$ cmake3 ..
$ make
$ ./ss5 -i -t <number of threads> <from> <to>
```

Additional information:

```
$./ss5 --help
```

Dependencies:

- pthread library

External libraries

- Templatized C++ Command Line Parser Library (TCLPL) - http://tclap.sourceforge.net/

#### check_if

A python script able to verify if a valuation satisfies a logical formula.

How to use:

```
$./python3 main.py <DIMACS file> <valuation file>
```

Additional information:

```
$./python3 main.py --help
```

### Literature

- Conflict-Driven Clause Learning SAT Solvers - https://www.ics.uci.edu/~dechter/courses/ics-275a/winter-2016/readings/SATHandbook-CDCL.pdf

---

by: Ziga Putrle
course: Logic in Computer science
year: 2017
