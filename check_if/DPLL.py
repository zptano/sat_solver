from expression import *
from copy import deepcopy

import random

def propagateUnits(formula):
    units = formula.getUnits()
    while len(units) != 0:
        for un in units:
            # Propagate the unit.
            st = formula.assumeTrue(un)

            # Check the state.
            if (st == Formula.State.UNSATISFIABLE or
                st == Formula.State.SATISFIED):
                return st

        units = formula.getUnits()

    return Formula.State.UNKNOWN

def DPLL(formula, level = 0):
    # Check he state of the formula.
    st = formula.getState()
    if (st == Formula.State.UNSATISFIABLE or
        st == Formula.State.SATISFIED):
        return st

    # Propagate units.
    st = propagateUnits(formula)

    if (st == Formula.State.UNSATISFIABLE or
        st == Formula.State.SATISFIED):
        return st

    # Conflict analysis.
    pass

    # Backtrack.
    pass

    # Branch.
    unLiterals = formula.getUnassignedLiterals()
    #print("level = " + str(level) + ", unLiterals = " + str([str(unLit) for unLit in unLiterals]))

    print("level = " + str(level) + ", asTrue = " +  str([str(asTr) for asTr in formula._assumedTrue]))

    #for unLit in unLiterals:
    while len(unLiterals) != 0:
        #print("unLit = " + str(unLit))


        cpFr = deepcopy(formula)

        unLit1 = unLiterals.pop(random.randrange(0,len(unLiterals)))
        cpFr.assumeTrue(unLit1)
        
        if len(unLiterals) > 0:
            unLit2 = unLiterals.pop(random.randrange(0,len(unLiterals)))
            cpFr.assumeTrue(unLit2)

        st = DPLL(cpFr, level + 1)
        if st == Formula.State.SATISFIED:
            return Formula.State.SATISFIED
        elif st == Formula.State.UNSATISFIABLE:
            continue
        else:
            raise RuntimeError("The Formula.State.UNKNOWN is not expected.")

    #print("OUT")

    return Formula.State.UNSATISFIABLE
